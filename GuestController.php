<?php

namespace App\Http\Controllers\Guest;


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Response;
use Illuminate\Support\Facades\Validator;
use App\Models\Feedback;
use App\Models\Order;
use App\Models\Author;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class GuestController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promotion_deadline = DB::table('system_settings')->where('type', '=','promo')->first();
        $promotion_deadline = !isset($promotion_deadline) || empty($promotion_deadline || strtotime($promotion_deadline) < time() ) ? mktime(0,0,0, date('m'), date('d') + 3, date('Y')) : $promotion_deadline;

        $feedbacks = Feedback::withRate(2)->moderated()->orderBy('created_at', 'desc')->take(6)->get();

        return view('public.index', [
            'promotion_deadline' => $promotion_deadline,
            'feedbacks'          => $feedbacks
        ]);
    }
    
    public function show_feedback($fid)
    {
        return view('public.feedback', [ 'feedback' => Feedback::where('fid', '=', $fid)->moderated()->first() ]);
    }

    public function add_feedback( Request $request )
    {
        if ($request->isMethod('post') || $request->isXmlHttpRequest()) {

            $validator = Validator::make($request->all(), [
                'name'    => 'required|min:3',
                'email'   => 'required|email',
                'rating'  => 'required|integer|min:0|max:5',
                'message' => 'required|min:3'
            ]);

            if (!$validator->fails()) {
                $data = array_merge(
                    $request->except(['_token', '_url']),
                    [
                        'updated_at' => date('Y-m-d H:i:s', time()),
                        'created_at' => date('Y-m-d H:i:s', time())
                    ]
                );
                
                $feedback = Feedback::create($data);
                
                if (isset($feedback->id) && !empty($feedback->id)) {
                    $success_screen = '<div class="title" id="feedback-success"><i class="fa-smile-o"></i></div>';
                    $success_screen .= '<div class="text"><h3>';
                    $success_screen .= trans('front.feedback_thanks') . '</h3></div>';

                    return Response::json(['status' => 'created', 'screen' => $success_screen]);
                } else {
                    return Response::json(['status' => 'error_insert']);
                }

            } else {
                return Response::json(['status' => 'invalid', 'errors' => $validator->errors()]);
            }
        }

        return Response::json(['status' => 'error']);
    }

    public function catalog( Request $request )
    {
        $categories = DB::table('category')->select('id', 'name', 'url')->where('active', '=', 1)->orderBy('name', 'ASC')->get();

        $posts = DB::table('posts AS p')
            ->select('p.pid AS id', 'p.name', 'p.url AS url', 'p.short_description', 'p.price', 'p.type', DB::raw('UNIX_TIMESTAMP(p.updated_at) AS updated_at'), 'a.name AS author', 'c.name as category')
            ->join('category AS c', 'c.id', '=', 'p.cid')
            ->join('users AS a', 'a.id', '=', 'p.author_id')
            ->where('p.status', '=', '200')
            ->where('p.active', '=', '1')
            ->where('p.delete', '=', '0')
            ->orderBy('p.updated_at', 'DESC')
            ->paginate(10);

        return view('public.catalog', [
            'posts'      => $posts,
            'categories' => $categories
        ]);
    }

    public function catalog_category( $cid = 0 )
    {
        $categories = DB::table('category')->select('id', 'name', 'url')->where('active', '=', 1)->orderBy('name', 'ASC')->get();

        $posts = DB::table('posts AS p')
            ->select('p.pid AS id', 'p.name', 'p.url AS url', 'p.short_description', 'p.price', 'p.type', DB::raw('UNIX_TIMESTAMP(p.updated_at) AS updated_at'), 'a.name AS author', 'c.name as category')
            ->join('category AS c', 'c.id', '=', 'p.cid')
            ->join('users AS a', 'a.id', '=', 'p.author_id')
            ->where('p.status', '=', '200')
            ->where('p.active', '=', '1')
            ->where('p.delete', '=', '0')
            ->where('p.cid', '=', $cid)
            ->orderBy('p.updated_at', 'DESC')
            ->paginate(10);

        if (count($categories) > 0 && !empty($categories)) {
            $cat = '';
            
            foreach ($categories as $category)
                if ($category['id'] == $cid) {
                    $cat = $category['name'];
                    break;
                }

            if (count($posts) > 0 && !empty($posts)) {
                $pageTitle = trans('front.found_in_category', ['cat'=>$cat]);
            } else {
                $pageTitle = trans('front.not_found_in_category', ['cat'=>$cat]);
            }
        } else {
            $pageTitle = trans('front.not_found_category');
        }

        return view('public.catalog', [
            'categories' => $categories,
            'posts'      => $posts,
            'pageTitle'  => $pageTitle,
            'cid'        => $cid
        ]);
    }

    public function post( $pid )
    {
        $post = DB::table('posts AS p')
            ->select('p.pid AS id', 'p.name', 'p.url AS url', 'p.description', 'p.price', 'p.type', DB::raw('UNIX_TIMESTAMP(p.updated_at) AS updated_at'), 'a.name AS author', 'c.name as category')
            ->join('category AS c', 'c.id', '=', 'p.cid')
            ->join('users AS a', 'a.id', '=', 'p.author_id')
            ->where('p.status', '=', '200')
            ->where('p.active', '=', '1')
            ->where('p.delete', '=', '0')
            ->where('p.pid', '=', $pid)
            ->orderBy('p.updated_at', 'DESC')
            ->first();

        return view('public.article', [
            'post' => $post
        ]);
    }

    public function search( Request $request )
    {
        $title = '';
        $query = '';

        $posts = DB::table('posts AS p')
            ->select('p.pid AS id', 'p.name', 'p.url AS url', 'p.short_description', 'p.price', 'p.type', DB::raw('UNIX_TIMESTAMP(p.updated_at) AS updated_at'), 'a.name AS author', 'c.name as category')
            ->join('category AS c', 'c.id', '=', 'p.cid')
            ->join('users AS a', 'a.id', '=', 'p.author_id')
            ->where('p.status', '=', '200')
            ->where('p.active', '=', '1')
            ->where('p.delete', '=', '0')
            ->where('p.name', 'LIKE', "%" . $request->input('search') . "%")
            ->orderBy('p.updated_at', 'DESC')
            ->paginate(10);

        if ($request->input('search')) {
            $title = trans('front.found', ['count' => $posts->count(), 'param' => $request->input('search')]);
            $query = $request->input('search');
        }

        return view('public.search', [
            'title' => $title,
            'query' => $query,
            'posts' => $posts
        ]);
    }

    public function authors()
    {
        $types = Config::get('order.type');
        $categories = DB::table('category')
            ->select('id', 'name', 'authors', 'updated_at', 'created_at')
            ->where('active', '=', 1)
            ->where('authors', '=', 1)
            ->paginate(20);

        return view('public.authors', [
            'types' => $types,
            'categories' => $categories
        ]);
    }

    public function add_authors( Request $request )
    {
        if ($request->isMethod('post') || $request->isXmlHttpRequest()) {

            $validator = Validator::make(Input::all(), [
                'name'         => 'required|min:3',
                'phone'        => 'required|min:3',
                'email'        => 'required|min:3',
                'skype'        => 'required|min:3',
                'education'    => 'required|min:3',
                'message'      => 'required|min:3',
                'works[]'      => 'array|size:1',
                'experience'   => 'required|min:3',
                'next_tasks[]' => 'array|size:1',
                'next_works[]' => 'array|size:1'
            ]);

            if (!$validator->fails()) {

                $order = Author::create(
                    [
                        'status'     => 100,
                        'name'       => Input::get('name'),
                        'phone'      => Input::get('phone'),
                        'email'      => Input::get('email'),
                        'skype'      => Input::get('skype'),
                        'message'    => Input::get('message'),
                        'education'  => Input::get('education'),
                        'experience' => Input::get('experience'),
                        'updated_at' => date('Y-m-d H:i:s', time()),
                        'created_at' => date('Y-m-d H:i:s', time()),
                        'works'      => implode(',', Input::get('works')),
                        'next_works' => implode(',', Input::get('next_works')),
                        'next_tasks' => implode(',', Input::get('next_tasks')),
                    ]
                );

                if (isset($order->id) && !empty($order->id)) {
                    $success_screen = '<div class="title"><i class="fa-smile-o"></i></div>';
                    $success_screen .= '<div class="text"><h3>';
                    $success_screen .= trans('front.authors_thanks') . '</h3></div>';

                    return Response::json(['status' => 'created', 'screen' => $success_screen]);
                } else {
                    return Response::json(['status' => 'error_insert']);
                }

            } else {
                return Response::json(['status' => 'invalid', 'errors' => $validator->errors()]);
            }
        }

        return Response::json(['status' => 'error']);
    }

    public function contact( Request $request )
    {
        return view('public.contact');
    }

    public function sitemap( Request $request )
    {
        return view('public.sitemap');
    }

    public function order( Request $request )
    {
        $promotion_deadline = DB::table('system_settings')->where('type', '=','promo')->first();
        $promotion_deadline = !isset($promotion_deadline) || empty($promotion_deadline || strtotime($promotion_deadline) < time() ) ? mktime(0,0,0, date('m'), date('d') + 3, date('Y')) : $promotion_deadline;

        $types = Config::get('order.type');

        return view('public.order', [
            'promotion_deadline' => $promotion_deadline,
            'types'              => $types
        ]);
    }

    public function add_order( Request $request )
    {
        if ($request->isMethod('post') || $request->isXmlHttpRequest()) {

            $validator = Validator::make(Input::all(), [
                'name'     => 'required|min:3',
                'phone'    => 'required|min:3',
                'email'    => 'required|min:3',
                'skype'    => 'required|min:3',
                'theme'    => 'required|min:3',
                'type'     => 'required',
                'location' => 'required|min:3',
                'school'   => 'required|min:3',
                'deadline' => 'required',
            ]);

            if (!$validator->fails()) {

                $allData = array_merge_recursive([
                    'status' => 100,
                    'updated_at' => date('Y-m-d H:i:s', time()),
                    'created_at' => date('Y-m-d H:i:s', time()),
                ], $request->except(['_token', '_url']));
                
                $order = Order::create($allData);

                if (isset($order->id) && !empty($order->id)) {
                    $success_screen = '<div class="title"><i class="fa-smile-o"></i></div>';
                    $success_screen .= '<div class="text"><h3>';
                    $success_screen .= trans('front.order_thanks') . '</h3></div>';

                    return Response::json(['status' => 'created', 'screen' => $success_screen]);
                } else {
                    return Response::json(['status' => 'error_insert']);
                }

            } else {
                return Response::json(['status' => 'invalid', 'errors' => $validator->errors()]);
            }
        }

        return Response::json(['status' => 'error']);
    }

    public function payment()
    {
        return view('public.payment');
    }

    public function pricing()
    {
        return view('public.pricing');
    }
}

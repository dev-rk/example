<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedback';
    protected $primaryKey = 'fid';
    protected $fillable = [
        'name', 'email', 'message', 'rating', 'publish', 'viewed'
    ];

    public function scopeWithRate($query, $positive = 3)
    {
        return $query->where('rating', '>', (int) $positive);
    }

    public function scopeModerated($query)
    {
        return $query->where('publish', '=', 1)->where('viewed', '=', 1);
    }
}

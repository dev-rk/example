<?php

namespace App\Helpers;

use App\Models\Record;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Calendar {
    
    public static function generateCalendar($onlyMonth = false)
    {
        $currentTime = Request::capture()->get('date') ? Request::capture()->get('date') : mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        
        $time = new \DateTime(strftime("%Y-%m-%d", $currentTime));
        $month_start = $time->modify('first day of this month')->getTimestamp();
        $from = $time->sub(new \DateInterval('P' . ($time->format('w') > 0 ? $time->format('w') - 1 : $time->format('w')) . 'D'))->getTimestamp();
        
        $time = new \DateTime(strftime("%Y-%m-%d", $currentTime));
        $month_end = $time->modify('first day of next month')->getTimestamp();
        $to = $time->add(new \DateInterval('P' . ($time->format('w') > 1 ? 6 - $time->format('w') : 0 ) . 'D'))->add(new \DateInterval('P6D'))->getTimestamp();
        
        $countRecords = Record::select(DB::raw('UNIX_TIMESTAMP(DATE(FROM_UNIXTIME(`creation_timestamp`))) as timestamp, count(*) as records'))
            ->where('lid', '=', Auth::user()->id)
            ->where('creation_timestamp', '>=', $month_start)
            ->where('creation_timestamp', '<', $month_end)
            ->groupBy(DB::raw('UNIX_TIMESTAMP(DATE(FROM_UNIXTIME(`creation_timestamp`)))'))
            ->get();
        
        $calendarRecords = [];
        
        foreach ($countRecords->toArray() as &$countRecord)
            $calendarRecords[strftime('%d/%m/%Y', $countRecord['timestamp'])] = $countRecord['records'];
        
        $calendar = [
            'date'    => strftime('%d', $currentTime),
            'month'   => strftime('%B', $currentTime),
            'monthRu' => self::monthRu((int) date('n', $currentTime)),
            'weekday' => strftime('%A', $currentTime),
        ];
        
        $week = 0;
        
        while ($from <= $to)
        {
            for ($i = 0; $i < 7; $i++)
            {
                $formatTime = strftime('%d/%m/%Y', $from);
                
                $calendar['days'][$week][$formatTime] = [
                    'label'  => strftime('%d', $from),
                    'class'  => $from < $month_end && $month_start <= $from ? (isset($calendarRecords[$formatTime]) ? 'btn-cl' : '') : 'disabled',
                    'count'  => isset($calendarRecords[$formatTime]) ? $calendarRecords[$formatTime] : 0,
                    'select' => !$onlyMonth && $formatTime == strftime('%d/%m/%Y', $currentTime) ? 'selected' : ''
                ];
                
                $from += 86400;
            }
            $week++;
        }
        
        return $calendar;
    }
    
    public static function monthRu($monthNum = 0)
    {
        $month = ['������', '�������', '�����', '������', '���', '����', '����', '�������', '��������', '�������', '������', '�������'];
        
        return (int) $monthNum > 0 && (int) $monthNum < 13
            ? $month[(int) $monthNum - 1]
            : $month[0];
    }
}
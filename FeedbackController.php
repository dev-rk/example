<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Route;
use Redirect;
use Validator;
use App\Models\Feedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;

class FeedbackController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function feedback_list()
    {
        $feedbacks = Feedback::orderBy('updated_at', 'desc')->paginate(20);

        return view('admin.feedback.list', [
            'feedbacks'  => $feedbacks,
        ]);
    }

    public function feedback( $id = 0 )
    {
        $item   = Feedback::where('fid', '=', $id)->first();

        if ($item['viewed'] != 1)
            $item->where('fid', '=', $id)->update(['viewed' => 1]);

        return view('admin.feedback.item', [
            'item'   => $item,
        ]);
    }

    public function upd_feedback( $request, $id = 0 )
    {
        $validator = Validator::make($request->all(), [
            'name'    => 'required|min:3',
            'email'   => 'email',
            'rating'  => 'required|integer|min:0|max:5',
            'message' => 'required|min:3'
        ]);

        if (!$validator->fails()) {

            $data = array_merge_recursive([
                'updated_at' => date('Y-m-d H:i:s', time()),
                'created_at' => date('Y-m-d H:i:s', time()),
            ], $request->except(['_token', '_url', '_method']));
    
            $data['publish'] = $request->get('publish') ? 1 : 0;
    
            Feedback::where('fid', '=', $id)->update($data);
        } else {
            return Redirect::to('control/feedback/' . $id)->withInput()->withErrors($validator);
        }

        return redirect('control/feedback/' . $id);
    }


    public function del_feedback( $id = 0 )
    {
        DB::table('feedback')->where('fid', '=', $id)->delete();

        return redirect('control/feedback_list/');
    }

    public function add_feedback( Request $request )
    {
        if ($request->isMethod('put')) {
            
            $validator = Validator::make($request->all(), [
                'name'    => 'required|min:3',
                'email'   => 'email',
                'rating'  => 'required|integer|min:0|max:5',
                'message' => 'required|min:3'
            ]);
            
            if (!$validator->fails()) {

                $data = array_merge_recursive([
                    'updated_at' => date('Y-m-d H:i:s', time()),
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'viewed'     => 1,
                ], $request->except('_token', '_url', '_method'));

                $feedback = Feedback::create($data);
                
                if (isset($feedback->fid) && !empty($feedback->fid)) {
                    return Redirect::to('control/feedback/' . $feedback->fid);
                }
                return Redirect::to('control/feedback/')->withInput()->withErrors($validator);
            } else {
                return Redirect::to('control/feedback/')->withInput()->withErrors($validator);
            }
        }

        return view('admin.feedback.new');
    }
}